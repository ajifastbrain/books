//
//  BooksModel.swift
//  Books
//
//  Created by Aji Prakosa on 15/06/21.
//

import ObjectMapper

class BooksModel: Mappable {
    var booksData: BooksData?
    var description: String?
    var status_code: Int?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        booksData <- map["data"]
        description <- map["description"]
        status_code <- map["status_code"]
    }
}


class BooksData: Mappable {
    var listBooks: [ListBooks]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        listBooks <- map["books"]
    }
}

class ListBooks: Mappable {
    var createdAt: String?
    var createdBy: CreatedBy?
    var description: String?
    var id: String?
    var modifiedAt: Int?
    var modifiedBy: ModifiedBy?
    var name: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        createdAt <- map["createdAt"]
        createdBy <- map["createdBy"]
        description <- map["description"]
        id <- map["id"]
        modifiedAt <- map["modifiedAt"]
        modifiedBy <- map["modifiedBy"]
        name <- map["name"]
    }
}

class CreatedBy: Mappable {
    var fullname: String?
    var id: String?
    var username: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        fullname <- map["fullname"]
        id <- map["id"]
        username <- map["username"]
    }
}

class ModifiedBy: Mappable {
    var fullname: String?
    var id: String?
    var username: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        fullname <- map["fullname"]
        id <- map["id"]
        username <- map["username"]
    }
}

