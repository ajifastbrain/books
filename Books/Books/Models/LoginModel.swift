//
//  LoginModel.swift
//  Books
//
//  Created by Aji Prakosa on 15/06/21.
//

import ObjectMapper

class LoginModel: Mappable {
    var data: Data?
    var description: String?
    var status_code: Int?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        data <- map["data"]
        description <- map["description"]
        status_code <- map["status_code"]
    }
}

class Data: Mappable {
    var token: String?
    var user: User?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        token <- map["token"]
        user <- map["user"]
    }
}

class User: Mappable {
    var active: Bool?
    var fullname: String?
    var id: String?
    var username: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        active <- map["active"]
        fullname <- map["fullname"]
        id <- map["id"]
        username <- map["username"]
    }
}
