//
//  DetailBookModel.swift
//  Books
//
//  Created by Aji Prakosa on 16/06/21.
//

import ObjectMapper

class DetailBookModel: Mappable {
    var detailData: ListBooks?
    var description: String?
    var status_code: Int?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        detailData <- map["data"]
        description <- map["description"]
        status_code <- map["status_code"]
    }
}

