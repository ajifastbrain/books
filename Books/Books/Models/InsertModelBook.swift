//
//  InsertModelBook.swift
//  Books
//
//  Created by Aji Prakosa on 16/06/21.
//

import ObjectMapper

class InsertModelBook: Mappable {
    var description: String?
    var status_code: Int?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        description <- map["description"]
        status_code <- map["status_code"]
    }
}
