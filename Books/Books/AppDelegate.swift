//
//  AppDelegate.swift
//  Books
//
//  Created by Macintosh on 15/06/21.
//

import UIKit
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        let rootVC = LoginViewController()
        
        let navController = BaseNavigationController(rootViewController: rootVC)
        window?.rootViewController = navController
        
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        instantiateViewController()
        return true
    }

    func instantiateViewController() {
        let prefs:UserDefaults = UserDefaults.standard
        let isLoggedIn:Int = prefs.integer(forKey: defaultsKeys.keyLogin) as Int
        
        if (isLoggedIn == 1) {
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.makeKeyAndVisible()
            
            let rootVC = HomeViewController()
            
            let navController = BaseNavigationController(rootViewController: rootVC)
            window?.rootViewController = navController
        } else {
            self.animate(vc: BaseNavigationController(rootViewController: LoginViewController()))
        }
    }

    private func animate(vc: UIViewController) {
        if let window = self.window {
            
            let snapShotView = window.snapshotView(afterScreenUpdates: true)
            
            vc.view.addSubview(snapShotView!)
            snapShotView?.layer.zPosition = 1000
            window.rootViewController = vc
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: { () -> Void in
                snapShotView?.alpha = 0.0
            }) { (finished) -> Void in
                snapShotView?.removeFromSuperview()
            }
            
            window.makeKeyAndVisible()
        }
    }


}

