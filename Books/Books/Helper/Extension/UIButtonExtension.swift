//
//  UIButtonExtension.swift
//  Books
//
//  Created by Aji Prakosa on 15/06/21.
//

import Foundation

extension UIButton {
    
    func mainButton() {
        layer.shadowColor = UIColor(hexString: "#000000").cgColor
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 8
        layer.shadowOffset = CGSize(width: 0, height: 6)
        
        layer.masksToBounds = false
        layer.cornerRadius = 25
    }
    
    func listButton() {
        layer.shadowColor = UIColor(hexString: "#000000").cgColor
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 2
        layer.shadowOffset = CGSize(width: 0, height: 1)
        
        layer.masksToBounds = false
        layer.cornerRadius = 5
    }
 }
