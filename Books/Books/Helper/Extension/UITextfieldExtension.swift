//
//  UITextfieldExtension.swift
//  Books
//
//  Created by Aji Prakosa on 15/06/21.
//

import Foundation

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }

    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func roundTextField() {
        layer.cornerRadius = 22
        layer.borderWidth = 1
        layer.borderColor = UIColor.white.cgColor
        setLeftPaddingPoints(15)
        setRightPaddingPoints(15)
    }

}
