//
//  StringExtension.swift
//  Books
//
//  Created by Aji Prakosa on 15/06/21.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    func safelyLimitedTo(length n: Int)->String {
        if (self.count <= n) {
            return self
        }
        return String( Array(self).prefix(upTo: n) )
    }

    func length() -> Int! {
        guard self.count > 0 else { return 0 }
        return self.count
    }
 }
