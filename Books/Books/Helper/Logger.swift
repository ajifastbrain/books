//
//  Logger.swift
//  Books
//
//  Created by Aji Prakosa on 15/06/21.
//

import Foundation
import Crashlytics

public class Logger {
    let debugMode = true
    
    class var shared: Logger {
        struct Static {
            static let instance: Logger = Logger()
        }
        return Static.instance
    }
    
    func d(_ obj: AnyObject) {
        if (debugMode) { debugPrint(obj) }
        
        let o = obj as! NSObject
        CLSLogv("%@", getVaList([o]))
    }
}

extension Logger {
    class func log(_ obj: Any) {
        Logger.shared.d(obj as AnyObject)
    }
}
