//
//  Strings.swift
//  Books
//
//  Created by Aji Prakosa on 15/06/21.
//

import Foundation

struct Localization {
    struct Label {
        static let LOGIN = "Login".localized
        static let SOMETHING_WENT_WRONG = "Something Went Wrong".localized
        static let LIST_BOOKS = "List Books".localized
        static let DETAIL_BOOKS = "Detail Book".localized
        static let EDITING_BOOKS = "Editing Book".localized
        static let INSERT_BOOKS = "Insert Book".localized
        static let WARNING = "Warning".localized
        static let SUCCESS_EDITING = "Editing Data is Succesfully".localized
        static let SUCCESS_CREATE = "Creating Data is Succesfully".localized
        static let DATA_EMPTY = "Title or Description is Empty".localized
        static let ERROR = "Error".localized
    }
}
