//
//  NetworkManager.swift
//  Books
//
//  Created by Aji Prakosa on 15/06/21.
//

import UIKit
import Alamofire

class NetworkManager: NSObject {
    class func request_formdata(_ method: HTTPMethod, _ URLString: URLConvertible, parameters: [String: AnyObject]? = nil, useBasicToken: Bool? = false, logoutIf401: Bool = true, completion: @escaping (DataResponse<Any>) -> Void) {
        
        let manager = Alamofire.SessionManager.default
        var headers = [String : String]()
        if useBasicToken! {
            headers["Authorization"] = "\(Configuration.Authorization())"
        } else {
            headers["Authorization"] = (Session.sharedInstance.token != nil)
                ? "\(String(describing: Session.sharedInstance.token!.data?.token))"
                : "\(Configuration.Authorization())"
        }
        
        let req = manager.request(URLString, method: method, parameters: parameters, headers: headers)
        req.responseString { (response) in
            if let response = response.response {
                Logger.log(response)
                if response.statusCode == 401 && logoutIf401 && Session.sharedInstance.token != nil
                {
                    NotificationCenter.default.post(name: NSNotification.Name(BooksNotification.tokenExpired), object: nil)
                    
                    return
                    
                } else {
                    req.responseJSON { (response) -> Void in
                        completion(response)
                    }
                }
            } else {
                req.responseJSON { (response) -> Void in
                    completion(response)
                }
            }
        }
    }

    
    class func request(_ method: HTTPMethod, _ URLString: URLConvertible, parameters: [String: AnyObject]? = nil, encoding: ParameterEncoding = URLEncoding.default, useBasicToken: Bool? = false, logoutIf401: Bool = true, completion: @escaping (DataResponse<Any>) -> Void) {
        
        let manager = Alamofire.SessionManager.default
        let headers = [String : String]()
        
        let req = manager.request(URLString, method: method, parameters: parameters, encoding: encoding, headers: headers)
        req.responseString { (response) in
            if let response = response.response {
                Logger.log(response)
                if response.statusCode == 401 && logoutIf401
                {
                    NotificationCenter.default.post(name: NSNotification.Name(BooksNotification.tokenExpired), object: nil)
                    
                    // USER SIGN OUT TODO
                    print("User Sign Out TODO")
                    return
                    
                } else {
                    req.responseJSON { (response) -> Void in
                        completion(response)
                    }
                }
            } else {
                req.responseJSON { (response) -> Void in
                    completion(response)
                }
            }
        }
    }
}
