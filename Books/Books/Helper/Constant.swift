//
//  Constant.swift
//  Books
//
//  Created by Aji Prakosa on 15/06/21.
//

import Foundation
import UIKit

struct defaultsKeys {
    static let keyLogin = "keyLogin"
    static let keyToken = "keyToken"
}

struct BooksNotification {
    static let tokenExpired     = "NOTIFICATION_TOKEN_EXPIRED"
    static let pinChanged       = "NOTIFICATION_PIN_CHANGED"
}

struct Size {
    static let headerHeight: CGFloat        = 65
    static let tabBarHeight: CGFloat        = 44
    static let statusBarHeight: CGFloat     = 20
    static let cardDivider: CGFloat         = 1.55
    static let actionButtonHeight: CGFloat  = 56
    static let tableCellHeight: CGFloat     = 56
    static let cardInfoViewHeight: CGFloat  = 30
    static let defaultPadding: CGFloat      = 16
}

struct APIEndPoint {
    static let login           = "users/login"
    static let books           = "books"
    static let bookDetail      = "books/detail"
    static let bookEdit        = "books/edit"
    static let bookInsert      = "books/insert"
    static let logout          = "users/logout"
}
