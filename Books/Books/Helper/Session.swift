//
//  Session.swift
//  Books
//
//  Created by Aji Prakosa on 15/06/21.
//

import Foundation
import Alamofire

class Session: NSObject {
    static var sharedInstance: Session {
        struct Static {
            static let instance = Session()
        }
        
        return Static.instance
    }
    
    var token: LoginModel?
}
