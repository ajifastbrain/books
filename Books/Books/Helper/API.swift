//
//  API.swift
//  Books
//
//  Created by Aji Prakosa on 15/06/21.
//

import UIKit
import Alamofire
import ObjectMapper

class API: NSObject {
    class func resultProcessor(response: (DataResponse<Any>), completion: (_ data: AnyObject?, _ error: String?) -> Void) {
        var apiError: String?
        var body: AnyObject?
        
        switch response.result {
        case .failure(let error):
            Logger.log("Error coy: \(error)")
            apiError = error.localizedDescription
        case .success(let json):
            Logger.log(json)
            if let httpResponse: HTTPURLResponse = response.response, httpResponse.statusCode == 200 || httpResponse.statusCode == 201 || httpResponse.statusCode == 204 {
                body = json as AnyObject?
            } else {
                if let JSON = json as? [ String : AnyObject ] {
                    if let errorDesc = JSON["error_description"] {
                        apiError = errorDesc as? String
                    } else if let errorMessage = JSON["message"] {
                        if errorMessage is [ String : AnyObject ] {
                            if let message = errorMessage["error"] as? String {
                                apiError = message
                            } else if let message = errorMessage["error_message"] as? String {
                                apiError = message
                            }
                        } else {
                            apiError = errorMessage as? String
                        }
                    } else if let error = JSON["error"] as? String {
                        apiError = error
                    } else {
                        apiError = Localization.Label.SOMETHING_WENT_WRONG
                    }
                } else {
                    apiError = Localization.Label.SOMETHING_WENT_WRONG
                }
            }
        }
        
        completion(body, apiError)
    }
    
    class func getLogin(username: String, password: String, completion: @escaping (_ loginModelList: LoginModel?, _ error: String?) -> Void) {
        let url = Configuration.baseURL() + APIEndPoint.login
        let param: [String : AnyObject] = [
            "username"       : username as AnyObject,
            "password"       : password as AnyObject
        ]

        Logger.log(url)
        NetworkManager.request_formdata(.post, url, parameters: param, useBasicToken: true, logoutIf401: true, completion: { (response) in
            API.resultProcessor(response: response, completion: { (json, error) in
                var loginModelList: LoginModel?
                var apiError: String?
                if let json = json {
                    loginModelList = Mapper<LoginModel>().map(JSONString: stringify(json: json))
                } else if let err = error {
                    apiError = err
                }
                completion(loginModelList, apiError)
            })
        })
    }
    
    class func getBooksList(token: String, completion: @escaping (_ dataBookslList: BooksModel?, _ error: String?) -> Void) {
        let url = Configuration.baseURL() + APIEndPoint.books + "?token=\(token)"
        Logger.log(url)
        
        NetworkManager.request(.get, url, encoding: JSONEncoding.default, completion: { (response) in
            API.resultProcessor(response: response, completion: { (json, error) in
                var dataBookslList: BooksModel?
                var apiError: String?
                if let json = json {
                    dataBookslList = Mapper<BooksModel>().map(JSONString: stringify(json: json))
                } else if let err = error {
                    apiError = err
                }
                completion(dataBookslList, apiError)
            })
        })
    }

    class func getBooksDetail(token: String, id: String, completion: @escaping (_ dataBookslDetail: DetailBookModel?, _ error: String?) -> Void) {
        let url = Configuration.baseURL() + APIEndPoint.bookDetail + "?token=\(token)" + "&id=\(id)"
        Logger.log(url)
        
        NetworkManager.request(.get, url, encoding: JSONEncoding.default, completion: { (response) in
            API.resultProcessor(response: response, completion: { (json, error) in
                var dataBookslDetail: DetailBookModel?
                var apiError: String?
                if let json = json {
                    dataBookslDetail = Mapper<DetailBookModel>().map(JSONString: stringify(json: json))
                } else if let err = error {
                    apiError = err
                }
                completion(dataBookslDetail, apiError)
            })
        })
    }
    
    class func getEditingBook(token: String, id: String, name: String, description: String, completion: @escaping (_ dataBookslDetail: DetailBookModel?, _ error: String?) -> Void) {
        let url = Configuration.baseURL() + APIEndPoint.bookEdit + "?token=\(token)"
        let param: [String : AnyObject] = [
            "id"            : id as AnyObject,
            "name"          : name as AnyObject,
            "description"   : description as AnyObject
        ]

        Logger.log(url)
        NetworkManager.request_formdata(.post, url, parameters: param, useBasicToken: true, logoutIf401: true, completion: { (response) in
            API.resultProcessor(response: response, completion: { (json, error) in
                var dataBookslDetail: DetailBookModel?
                var apiError: String?
                if let json = json {
                    dataBookslDetail = Mapper<DetailBookModel>().map(JSONString: stringify(json: json))
                } else if let err = error {
                    apiError = err
                }
                completion(dataBookslDetail, apiError)
            })
        })
    }

    class func getCreateBook(token: String, name: String, description: String, completion: @escaping (_ createBook: InsertModelBook?, _ error: String?) -> Void) {
        let url = Configuration.baseURL() + APIEndPoint.bookInsert + "?token=\(token)"
        let param: [String : AnyObject] = [
            "name"          : name as AnyObject,
            "description"   : description as AnyObject
        ]

        Logger.log(url)
        NetworkManager.request_formdata(.post, url, parameters: param, useBasicToken: true, logoutIf401: true, completion: { (response) in
            API.resultProcessor(response: response, completion: { (json, error) in
                var createBook: InsertModelBook?
                var apiError: String?
                if let json = json {
                    createBook = Mapper<InsertModelBook>().map(JSONString: stringify(json: json))
                } else if let err = error {
                    apiError = err
                }
                completion(createBook, apiError)
            })
        })
    }

    class func getLogout(token: String, completion: @escaping (_ dataLogout: LogoutModel?, _ error: String?) -> Void) {
        let url = Configuration.baseURL() + APIEndPoint.logout + "?token=\(token)"
        Logger.log(url)
        
        NetworkManager.request(.get, url, encoding: JSONEncoding.default, completion: { (response) in
            API.resultProcessor(response: response, completion: { (json, error) in
                var dataLogout: LogoutModel?
                var apiError: String?
                if let json = json {
                    dataLogout = Mapper<LogoutModel>().map(JSONString: stringify(json: json))
                } else if let err = error {
                    apiError = err
                }
                completion(dataLogout, apiError)
            })
        })
    }


    static func stringify(json: Any, prettyPrinted: Bool = false) -> String {
        var options: JSONSerialization.WritingOptions = []
        if prettyPrinted {
          options = JSONSerialization.WritingOptions.prettyPrinted
        }

        do {
          let data = try JSONSerialization.data(withJSONObject: json, options: options)
          if let string = String(data: data, encoding: String.Encoding.utf8) {
            return string
          }
        } catch {
          print(error)
        }

        return ""
    }

}

