//
//  Connectivity.swift
//  Books
//
//  Created by Aji Prakosa on 15/06/21.
//

import Foundation
import Alamofire

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
