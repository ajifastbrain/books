//
//  HomeViewController.swift
//  Books
//
//  Created by Aji Prakosa on 15/06/21.
//

import UIKit

class HomeViewController: BaseViewController {
    @IBOutlet weak var tableBooks: UITableView!
    
    private var defaults = UserDefaults.standard
    
    private var booksListModel: BooksModel?
    
    private var listAllData: [ListBooks]?
    
    private var activationKey = String()
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.loadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onCreate(sender:)))
      
        self.navigationItem.rightBarButtonItems = [add]

        self.title = Localization.Label.LIST_BOOKS
        
        tableBooks.register(UINib(nibName: "ListBookViewCell", bundle: nil), forCellReuseIdentifier: "ListBookViewCell")
        
        tableBooks.delegate = self
        tableBooks.dataSource = self
        tableBooks.separatorStyle = .none
   }
    
   @objc func onDetail(sender: UIButton) {
      let id: String = self.listAllData?[sender.tag].id ?? ""
      self.navigationController?.pushViewController(DetailViewController(id: id), animated: true)
   }

   @objc func onEditing(sender: UIButton) {
       let id: String = self.listAllData?[sender.tag].id ?? ""
       let name: String = self.listAllData?[sender.tag].name ?? ""
       let desc: String = self.listAllData?[sender.tag].description ?? ""
 
       self.navigationController?.pushViewController(EditBookViewController(id: id, name: name, desc: desc), animated: true)
   }
    
    @objc func onCreate(sender: UIButton) {
        self.navigationController?.pushViewController(CreateBookViewController(), animated: true)
    }
    
    
    @IBAction func logoutTapped(_ sender: UIButton) {
        self.activationKey = self.defaults.string(forKey: defaultsKeys.keyToken) ?? ""
        API.getLogout(token: self.activationKey, completion: { (dataLogout, error) in
             if let dataLogout = dataLogout {
                 print(dataLogout)
                 self.hideLoadingView()
                 self.defaults.set(0, forKey: defaultsKeys.keyLogin)
                 self.defaults.set("", forKey: defaultsKeys.keyToken)
                 if let delegate = UIApplication.shared.delegate as? AppDelegate {
                    delegate.instantiateViewController()
                 }
             } else if let error = error {
                self.hideLoadingView()
                self.showAlertWith(title: "Error", message: error, action: nil)
             }
        })
    }
    
    
   func loadData() {
        self.showLoadingView()
        self.activationKey = self.defaults.string(forKey: defaultsKeys.keyToken) ?? ""
        API.getBooksList(token: self.activationKey, completion: { (dataBookslList, error) in
             if let booksData = dataBookslList {
                 self.booksListModel = booksData
                 self.listAllData = self.booksListModel?.booksData?.listBooks
                 self.tableBooks.reloadData()
                 self.hideLoadingView()
             } else if let error = error {
                self.hideLoadingView()
                self.showAlertWith(title: "Error", message: error, action: nil)
             }
        })

    }
}


extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listAllData?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        cell = tableView.dequeueReusableCell(withIdentifier: "ListBookViewCell", for: indexPath as IndexPath) as! ListBookViewCell
        let cells = cell as! ListBookViewCell
        let titles: String = self.listAllData?[indexPath.row].name ?? ""
        let desc: String = self.listAllData?[indexPath.row].description ?? ""
        cells.configureView(title: titles, desc: desc)
        cells.btnDetail.tag = indexPath.row
        cells.btnEdit.tag = indexPath.row
        
        cells.btnDetail.addTarget(self, action: #selector(onDetail(sender:)), for: .touchUpInside)
        cells.btnEdit.addTarget(self, action: #selector(onEditing(sender:)), for: .touchUpInside)
        
        cells.selectionStyle = .none
        return cell!
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 154
    }

}
