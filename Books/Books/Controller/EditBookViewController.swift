//
//  EditBookViewController.swift
//  Books
//
//  Created by Aji Prakosa on 16/06/21.
//

import UIKit

class EditBookViewController: BaseViewController {
    
    @IBOutlet weak var txtTitle: JVFloatLabeledTextField!
    
    @IBOutlet weak var txtDescription: JVFloatLabeledTextField!
    
    private var defaults = UserDefaults.standard
    
    @IBOutlet weak var btnEdit: UIButton!
    
    private var id = String()
    
    private var name = String()
    
    private var desc = String()
    
    private var booksDetailModel: DetailBookModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Localization.Label.EDITING_BOOKS

        txtTitle.roundTextField()
        txtDescription.roundTextField()
        
        txtTitle.text = self.name
        txtDescription.text = self.desc
        
        btnEdit.mainButton()
        self.view.endEditing(true)
        
    }

    @IBAction func editingTapped(_ sender: UIButton) {
        self.showLoadingView()
        let activationKey = self.defaults.string(forKey: defaultsKeys.keyToken)
        API.getEditingBook(token: activationKey ?? "", id: self.id, name: self.txtTitle.text ?? "", description: self.txtDescription.text ?? "", completion: { (dataBookslDetail, error) in
             if let dataBookslDetail = dataBookslDetail {
                 print(dataBookslDetail)
                 let alert = UIAlertController(title: Localization.Label.WARNING, message: Localization.Label.SUCCESS_EDITING, preferredStyle: .alert)
                 alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                 self.present(alert, animated: true, completion: nil)
                 self.hideLoadingView()
             } else if let error = error {
                self.hideLoadingView()
                self.showAlertWith(title: "Error", message: error, action: nil)
             }
        })
    }
    

    convenience init(id: String, name: String, desc: String) {
        self.init()
        self.id = id
        self.name = name
        self.desc = desc
    }

}
