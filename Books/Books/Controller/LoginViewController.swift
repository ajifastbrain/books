//
//  LoginViewController.swift
//  Books
//
//  Created by Macintosh on 15/06/21.
//

import UIKit

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var txtUsername: JVFloatLabeledTextField!
    
    @IBOutlet weak var txtPassword: JVFloatLabeledTextField!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    private var LoginListModels: LoginModel?
    
    private var defaults = UserDefaults.standard
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtUsername.roundTextField()
        txtPassword.roundTextField()
        btnLogin.mainButton()
        self.view.endEditing(true)
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        self.showLoadingView()
        API.getLogin(username: self.txtUsername.text ?? "", password: self.txtPassword.text ?? "", completion: { (loginModelList, error) in
             if let dataLogin = loginModelList {
                 self.LoginListModels = dataLogin
                 self.hideLoadingView()
                
                 self.defaults.set(dataLogin.data?.token, forKey: defaultsKeys.keyToken)
                 self.defaults.set(1, forKey: defaultsKeys.keyLogin)
                 self.navigationController?.navigationBar.isHidden = false
                 self.navigationController?.pushViewController(HomeViewController(), animated: true)
             } else if let error = error {
                self.hideLoadingView()
                self.showAlertWith(title: "Error", message: error, action: nil)
             }
        })
    }
}
