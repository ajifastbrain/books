//
//  CreateBookViewController.swift
//  Books
//
//  Created by Aji Prakosa on 16/06/21.
//

import UIKit

class CreateBookViewController: BaseViewController {
    
    @IBOutlet weak var txtTitle: JVFloatLabeledTextField!
    
    @IBOutlet weak var txtDesc: JVFloatLabeledTextField!
    
    @IBOutlet weak var btnCreate: UIButton!
    
    private var defaults = UserDefaults.standard
    
    private var booksCreateModel: InsertModelBook?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = Localization.Label.INSERT_BOOKS

        txtTitle.roundTextField()
        txtDesc.roundTextField()
        
        btnCreate.mainButton()
        self.view.endEditing(true)

    }

    @IBAction func btnCreateTapped(_ sender: UIButton) {
        if self.txtTitle.text == "" || self.txtDesc.text == "" {
            let alert = UIAlertController(title: Localization.Label.ERROR, message: Localization.Label.DATA_EMPTY, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            self.showLoadingView()
            let activationKey = self.defaults.string(forKey: defaultsKeys.keyToken)
            API.getCreateBook(token: activationKey ?? "", name: self.txtTitle.text ?? "", description: self.txtDesc.text ?? "", completion: { (createBook, error) in
                 if let createBook = createBook {
                     print(createBook)
                     let alert = UIAlertController(title: Localization.Label.WARNING, message: Localization.Label.SUCCESS_CREATE, preferredStyle: .alert)
                     alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                     self.present(alert, animated: true, completion: nil)
                     self.hideLoadingView()
                 } else if let error = error {
                    self.hideLoadingView()
                    self.showAlertWith(title: "Error", message: error, action: nil)
                 }
            })
        }
    }
    

}
