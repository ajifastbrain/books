//
//  DetailViewController.swift
//  Books
//
//  Created by Aji Prakosa on 16/06/21.
//

import UIKit

class DetailViewController: BaseViewController {
    
    private var id = String()
    
    private var defaults = UserDefaults.standard
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var labelDesc: UILabel!
    
    @IBOutlet weak var labelCreatedBy: UILabel!
    
    private var booksDetailModel: DetailBookModel?
    
    override func viewWillAppear(_ animated: Bool) {
        self.loadData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Localization.Label.DETAIL_BOOKS
        
        self.labelDesc.numberOfLines = 0
        self.labelDesc.lineBreakMode = .byWordWrapping
    }
    
    func loadData() {
         self.showLoadingView()
         let activationKey = self.defaults.string(forKey: defaultsKeys.keyToken)
         API.getBooksDetail(token: activationKey ?? "", id: id, completion: { (dataBookslDetail, error) in
              if let dataBookslDetail = dataBookslDetail {
                  self.booksDetailModel = dataBookslDetail
                  self.labelName.text = "Book Title : " + (self.booksDetailModel?.detailData?.name)!
                  self.labelDesc.text = "Description : " + (self.booksDetailModel?.detailData?.description)!
                  self.labelCreatedBy.text = "Created By = " + (self.booksDetailModel?.detailData?.createdBy?.fullname)!
            
                  self.hideLoadingView()
              } else if let error = error {
                 self.hideLoadingView()
                 self.showAlertWith(title: "Error", message: error, action: nil)
              }
         })

     }


    convenience init(id: String) {
        self.init()
        self.id = id
    }

}
