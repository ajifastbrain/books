//
//  ListBookViewCell.swift
//  Books
//
//  Created by Macintosh on 16/06/21.
//

import UIKit

class ListBookViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var DescLabel: UILabel!
    @IBOutlet weak var btnDetail: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var viewFrame: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DescLabel.lineBreakMode = .byWordWrapping
        DescLabel.numberOfLines = 0
        
        btnDetail.listButton()
        btnEdit.listButton()
  
        viewFrame.layer.cornerRadius = 15
        viewFrame.layer.borderWidth = 1
        viewFrame.layer.borderColor = UIColor.gray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureView(title: String, desc: String) {
        self.titleLabel.text = title
        self.DescLabel.text = desc
    }
    
}
